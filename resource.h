/*
 *	PC Name Grabber: A simple program that returns the name of the current
 *	PC on which the user is working.
 *
 *	Copyright (C) 2012  Ken Herbert
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Email:		admin@firedancer-software.com
 *	Website:	http://firedancer-software.com
 *
 */


// Images & Icons
#define IDI_APPICON 101

// Menu
#define IDR_APPMENU 201

// Menu Controls
#define ID_FILE_EXIT 2001
#define ID_HELP_WEBSITE 2002
#define ID_HELP_ABOUT 2003

// Dialogs
#define IDD_ABOUT 801
#define IDD_LICENCE 802

#define IDC_STATIC -1
